import * as wecco from "@wecco/core"

import * as models from "./models"
import { update} from "./controller"
import { root } from "./views"

document.addEventListener("DOMContentLoaded", async () => {
    wecco.app(() => new models.Model(0), update, root, "#app")
})
