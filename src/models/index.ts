
export class Model {
    constructor(public readonly count: number) {}

    increment(): Model {
        return new Model(this.count + 1)
    }

    decrement(): Model {
        return new Model(this.count - 1)
    }
}