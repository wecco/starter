import * as wecco from "@wecco/core"

import { Model } from "src/models"

export class Increment {
    readonly command = "inc"
}

export class Decrement {
    readonly command = "dec"
}

export type Message = Increment | Decrement

export function update(model: Model, message: Message, context: wecco.AppContext<Message>): Model {
    switch (message.command) {
        case "inc":
            return model.increment()
        case "dec":
            return model.decrement()
    }
}