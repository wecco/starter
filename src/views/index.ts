import * as wecco from "@wecco/core"

import { Decrement, Increment, Message } from "src/controller"
import { Model } from "src/models"

export function root (model: Model, context: wecco.AppContext<Message>): wecco.ElementUpdate {
    return wecco.html`
        <button @click=${() => context.emit(new Increment())}>+1</button>
        ${model.count}
        <button @click=${() => context.emit(new Decrement())}>-1</button>
    `
}
