# wecco-starter

A starter for single page apps using the `wecco` framework. 

This repo contains a running application which includes nothing but the framework and typescript. 
Use this repo as a starting point for your own single page applications.

## How to get started?

1. Clone this project
1. Update the git remote URL
1. Update the project's name in `package.json`
1. Update the project's title in `public/index.html`
1. Run `npm i`
1. Run `npm start`
1. Open [localhost:9999](http://localhost:9999/) in your favorite browser
1. Change the code

## What's in the box?

* [@wecco/core](https://bitbucket.org/wecco/core/)
* Typescript and ts-config
* Webpack incl. dev server

## License

This project is licensed under the Apache License V2.